<?php
/*
Version: 1.0
Plugin Name: DefaultCreationDateToToday
Plugin URI: 
Author: pokmis
Description: Automatically set the "Creation Date" of uploaded images that have no DateTimeOriginal EXIF data to the date/time they were uploaded.
If you like to organize your albums by Creation Date but sometimes upload images with no EXIF data this extension will default the creation date to the time they were uploaded so they show up in a meaningful order.
*/

// DCDTT = DefaultCreationDateToToday

// Check whether we are indeed included by Piwigo.
if (!defined('PHPWG_ROOT_PATH')) die('Hacking attempt!');
 
// Define the path to our plugin.
define('DCDTT_PATH', PHPWG_PLUGINS_PATH.basename(dirname(__FILE__)).'/');

// Hook on to an event to show the administration page.
add_event_handler('get_admin_plugin_menu_links', 'dcdtt_admin_menu');

// Add an entry to the 'Plugins' menu.
function dcdtt_admin_menu($menu) {
 array_push(
   $menu,
   array(
     'NAME'  => 'DefaultCreationDateToToday',
     'URL'   => get_admin_plugin_menu_link(dirname(__FILE__)).'/admin.php'
   )
 );
 return $menu;
}

add_event_handler('format_exif_data', 'dcdtt_format_exif_data');
function dcdtt_format_exif_data($exif, $filepath) {
  if (!isset($exif['DateTimeOriginal']))
  {
    $exif['DateTimeOriginal'] = date('Y:m:d H:i:s');
  }
  return $exif;
}

?>