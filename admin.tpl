<!-- Show the title of the plugin -->
<div class="titlePage">
 <h2>{'Default Creation Date To Today Plugin'|@translate}</h2>
</div>
 
<!-- Show content in a nice box -->
<fieldset>
 <legend>{'About'|@translate}</legend>
 
 {'Automatically set the "Creation Date" of uploaded images that have no DateTimeOriginal EXIF data to the date/time they were uploaded.'|@translate}
</fieldset>